# FluxUnicornHelp

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.14.

## Usage

Flux Unicorn Help is plugin, which can be used to integrate pre compiled html based help files in a popup.

### Installing

For installation just use the following command:

`npm i flux-unicorn-help`

Add the module to your main module.ts File

    FluxUnicornHelpModule

### Including Assets

To access the created help files, add the following line to your angular.json
`{
"glob": "**/*",
"input": "./node_modules/flux-unicorn-help/assets",
"output": "assets/"
}`

## Usage
To use the help Button just add the following line to your code:
`<flux-help-button [helpPageId]="yourHelpContextId"></flux-help-button>`

###Parameter
helpPageId - The ID of the Helppage
## Build

Run `ng build flux-unicorn-help --prod` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing
After building your library with `ng build flux-unicorn-help --prod`, go to the dist folder `cd dist/flux-unicorn-help` and run `npm publish`.

