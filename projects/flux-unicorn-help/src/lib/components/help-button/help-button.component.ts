import { Component, Input, OnInit } from '@angular/core';


@Component({
    selector: 'flux-help-button',
    templateUrl: './help-button.component.html',
    styleUrls: ['./help-button.component.scss']
})
export class HelpButtonComponent  implements OnInit {
    @Input() helpPageId;

    windowFeatures = 'menubar=no,location=no,resizable=no,scrollbars=no,status=no';

    constructor() {

    }

    ngOnInit() {}

    openHelpPopup() {
        window.open('assets/de/context/' + this.helpPageId + '.html', 'Flux Hilfe', this.windowFeatures);
    }
}
