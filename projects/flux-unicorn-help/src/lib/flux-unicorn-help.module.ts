import { NgModule } from '@angular/core';
import { HelpButtonComponent } from './components/help-button/help-button.component';


@NgModule({
  declarations: [
    HelpButtonComponent
  ],
  imports: [
  ],
  exports: [
    HelpButtonComponent
  ]
})
export class FluxUnicornHelpModule { }
